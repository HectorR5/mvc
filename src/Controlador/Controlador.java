/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Fecha;
import Vista.vdlgFecha;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Hector Ramirez
 */
public class Controlador implements ActionListener{
    
    private Fecha hoy;
    private vdlgFecha vista;

    public Controlador(Fecha hoy, vdlgFecha vista) {
        this.hoy = hoy;
        this.vista = vista;
        
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
    }
    
    private void iniciarVista(){
        vista.setTitle("Fecha");
          vista.setVisible(true);
    } 
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource()==vista.btnGuardar){
            hoy.setDia(Integer.parseInt(vista.txtDia.getText()));
            hoy.setMes(Integer.parseInt(vista.txtMes.getText()));
            hoy.setAño(Integer.parseInt(vista.txtAño.getText()));
        }
        
        if(e.getSource()==vista.btnMostrar){
            System.out.println("En mostrar");
            vista.txtDia.setText(String.valueOf(hoy.getDia()));
            vista.txtMes.setText(String.valueOf(hoy.getMes()));
            vista.txtAño.setText(String.valueOf(hoy.getAño()));
            if(hoy.isBisiesto()==true) JOptionPane.showMessageDialog(vista, "Es un año Bisiesto");
        }
        
        if(e.getSource()==vista.btnLimpiar){
            vista.txtDia.setText("");
            vista.txtMes.setText("");
            vista.txtAño.setText("");
        }
        
    }
    
    public static void main(String[] args){
        
        Fecha hoy = new Fecha();
        vdlgFecha vista = new vdlgFecha(new JFrame(),true);
        
        Controlador control = new Controlador(hoy,vista);
        control.iniciarVista();
        
    }
}
